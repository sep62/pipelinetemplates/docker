# Docker


## Getting started
Put the following in your .gitlab-ci.yml to get a full standard pipeline:

```yml
variables:
  CONTAINER_VERSION: "1.0.0"

include:
  - project: "kamstrup/PipelineTemplates/docker"
    ref: "TAG"
    file: "pipeline.yml"
```

<OR>

If you want to tag the publish image only with latest tag:
  
```yml
variables:
  ONLY_BRANCH_LATEST_TAG: "true"

include:
  - project: "kamstrup/PipelineTemplates/docker"
    ref: "TAG"
    file: "pipeline.yml"
```
Where:

- `TAG` is replaced with a tag from the tag list, for example `v1.1.0`.

### Optional Variables
<details>
<summary>Toggle to see variables that are optional and can be defined in either the gitlab-ci.yml or project scope variables if needed.</summary>


| variable name         | description                                                                                                                                                                                              | default value                            |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------|
| DOCKER_IMAGE_NAME     | The image name for CI, must be something with knowledge of docker in docker                                                                                                                              | `docker`                                 |
| DOCKER_IMAGE_VERSION  | The image version for CI                                                                                                                                                                                 | `20.10.12-dind`                          |
| DOCKERFILE_PATH       | The filepath to the dockerfile you want to build                                                                                                                                                         | `Dockerfile`                             |
| DOCKER_BUILD_PATH     | The filepath to the directory from where you want to build your dockerfile                                                                                                                               | `.`                                      |
| BUILD_IMAGE_NAME      | Image name of the container being created by the build step and published to your repo. This is somewhat the only way of sharing the container with the following job. This value should not be changed. | `$CI_REGISTRY_IMAGE/temp:$CI_COMMIT_SHA` |
| `PUBLISH_IMAGE_NAME`  | The published image name                                                                                                                                                                                 | "$CI_REGISTRY_IMAGE:$CONTAINER_VERSION"  |
| `MANUAL_PUBLISH`      | Set this variable to `true` if you want manual publish step.                                                                                                                                             |                                          |
| `BUILD_EXTRA_OPTIONS` | Extra options for docker build                                                                                                                                                                           |                                          |
| `BRANCH_LATEST_TAG`   | Set this variable to enable a branch specific latest tag. If this is enabled a $BUILD_IMAGE_NAME/$CI_COMMIT_BRANCH:latest is created on_success on master or release branches                            |                                          |
| `MANUAL_PUBLISH`      | Set this variable to enable manual publish                                                                                                                                                               |                                          |
| `BUILD_ARGS`          | Set build-time variables                                                                                                                                                                                 |                                          |
| `ONLY_BRANCH_LATEST_TAG`          | Push image only with latest tag from git default branch.                                                                                                                                                                                 |                                          |
</details>


